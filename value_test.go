package env_test

import (
	"errors"
	"testing"
	"time"

	"gitlab.com/slxh/go/env"
)

var (
	valueTestBool     bool
	valueTestDuration time.Duration
	valueTestTime     time.Time
	valueTestFloat32  float32
	valueTestFloat64  float64
	valueTestInt      int
	valueTestInt8     int8
	valueTestInt16    int16
	valueTestInt32    int32
	valueTestInt64    int64
	valueTestString   string
	valueTestUint     uint
	valueTestUint8    uint8
	valueTestUint16   uint16
	valueTestUint32   uint32
	valueTestUint64   uint64
)

var valueTests = []struct {
	Value                env.Value
	Default, Set, String string
	Get                  any
	Err                  error
}{
	{
		Value:   env.NewValueVar(&valueTestBool, true),
		Default: "true",
		Set:     "false",
		Get:     false,
		String:  "false",
	},
	{
		Value:   env.NewValueVar(&valueTestDuration, 1*time.Second),
		Default: "1s",
		Set:     "5h",
		Get:     5 * time.Hour,
		String:  "5h0m0s",
	},
	{
		Value:   env.NewValueVar(&valueTestTime, time.Time{}),
		Default: "0001-01-01 00:00:00 +0000 UTC",
		Set:     "2021-02-28T16:56:53Z",
		Get:     time.Date(2021, time.February, 28, 16, 56, 53, 0, time.UTC),
		String:  "2021-02-28 16:56:53 +0000 UTC",
	},
	{
		Value:   env.NewValueVar(&valueTestFloat32, float32(-1e6)),
		Default: "-1e+06",
		Set:     "15e6",
		Get:     float32(15e6),
		String:  "1.5e+07",
	},
	{
		Value:   env.NewValueVar(&valueTestFloat64, -1e9),
		Default: "-1e+09",
		Set:     "15e6",
		Get:     15e6,
		String:  "1.5e+07",
	},
	{
		Value:   env.NewValueVar(&valueTestInt, -123),
		Default: "-123",
		Set:     "1234",
		Get:     1234,
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestInt8, int8(-123)),
		Default: "-123",
		Set:     "127",
		Get:     int8(127),
		String:  "127",
	},
	{
		Value:   env.NewValueVar(&valueTestInt16, int16(-123)),
		Default: "-123",
		Set:     "1234",
		Get:     int16(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestInt32, int32(-123)),
		Default: "-123",
		Set:     "1234",
		Get:     int32(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestInt64, int64(-123)),
		Default: "-123",
		Set:     "1234",
		Get:     int64(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestString, "default"),
		Default: "default",
		Set:     "changed",
		Get:     "changed",
		String:  "changed",
	},
	{
		Value:   env.NewValueVar(&valueTestUint, uint(123)),
		Default: "123",
		Set:     "1234",
		Get:     uint(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestUint8, uint8(123)),
		Default: "123",
		Set:     "254",
		Get:     uint8(254),
		String:  "254",
	},
	{
		Value:   env.NewValueVar(&valueTestUint16, uint16(123)),
		Default: "123",
		Set:     "1234",
		Get:     uint16(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestUint32, uint32(123)),
		Default: "123",
		Set:     "1234",
		Get:     uint32(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestUint64, uint64(123)),
		Default: "123",
		Set:     "1234",
		Get:     uint64(1234),
		String:  "1234",
	},
	{
		Value:   env.NewValueVar(&valueTestUint64, uint64(123)),
		Default: "123",
		Set:     "-1234",
		Err:     errors.New("cannot parse uint64: strconv.ParseUint: parsing \"-1234\": invalid syntax"),
	},
	{
		Value: env.NewFuncValue(func(s string) error {
			if s == "yes" {
				return nil
			}
			return errors.New("no")
		}),
		Set: "yes",
	},
	{
		Value:   env.ValueFromGetter(new(testFlag), "default"),
		Default: "default",
		Get:     "test-flag",
		String:  "test-flag",
	},
	{
		Value:   env.ValueFromFlag(new(testFlag), "default"),
		Default: "default",
		Get:     nil,
		String:  "test-flag",
	},
}

type testFlag struct{}

func (t testFlag) String() string {
	return "test-flag"
}

func (t testFlag) Set(_ string) error {
	return nil
}

func (t testFlag) Get() any {
	return "test-flag"
}

func TestValue(t *testing.T) {
	for _, test := range valueTests {
		if test.Value.Default() != test.Default {
			t.Errorf("Wrong default: expected %q, got %q", test.Default, test.Value.Default())
		}

		err := test.Value.Set(test.Set)
		if err != nil {
			if test.Err == nil || err.Error() != test.Err.Error() {
				t.Errorf("Wrong EnvSet error: expected %q, got %q", test.Err, err)
			}

			continue
		}

		if test.Value.String() != test.String {
			t.Errorf("Wrong string: expected %q, got %q", test.String, test.Value.String())
		}

		if test.Get != test.Value.Get() {
			t.Errorf("Wrong Get: expected %#v, got %#v", test.Get, test.Value.Get())
		}
	}
}
