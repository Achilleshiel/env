//nolint:testpackage // These tests manipulate internal state.
package env

import (
	"bytes"
	"fmt"
	"testing"
)

func cleanup() {
	defaultSet = NewEnvSet("", ReturnFirstError)
}

func TestAdd_Lookup(t *testing.T) {
	defer cleanup()

	val := -5
	name := "TEST"
	usage := "test me"

	v := NewValueVar(&val, val)
	Var(v, name, usage)
	e := Lookup(name)

	if e.Value != v {
		t.Errorf("Incorrect value: expected %#v, got %#v", v, e.Value)
	}

	if e.Name != name {
		t.Errorf("Incorrect name: expected %q, got %q", name, e.Name)
	}

	if e.Usage != usage {
		t.Errorf("Incorrect UsageString: expected %q, got %q", usage, e.Usage)
	}
}

func TestParse(t *testing.T) {
	defer cleanup()

	var i int

	t.Setenv("TEST_PARSE_VAR", "-123")
	SetPrefix("TEST_PARSE_")
	Var(NewValueVar(&i, 0), "VAR", "")
	Parse()

	if i != -123 {
		t.Errorf("Parse incorrect: expected %v, got %v", -123, i)
	}
}

func TestParseWithFlags(t *testing.T) {
	defer cleanup()

	var i int

	t.Setenv("VAR", "not a number")

	AddVar(&i, "VAR", 0, "")

	if err := ParseWithFlags(); err == nil {
		t.Error("Expected error, got nil")
	}
}

func TestPrintDefaults(t *testing.T) {
	defer cleanup()

	var buf bytes.Buffer

	defaultSet.output = &buf

	Add("A", 5, "A")
	Add("B", 0, "B")
	Add("C", "", "C")
	Add("D", "x", "D")

	PrintDefaults()

	exp := "  A int\n" +
		"    \tA (default 5)\n" +
		"  B int\n" +
		"    \tB\n" +
		"  C string\n" +
		"    \tC\n" +
		"  D string\n" +
		"    \tD (default \"x\")\n"
	if buf.String() != exp {
		t.Errorf("Expected %q, got %q", exp, buf.String())
	}
}

func TestSetErrorHandling(t *testing.T) {
	SetErrorHandling(ReturnLastError)

	if defaultSet.errorHandling != ReturnLastError {
		t.Errorf("Incorrect error handling")
	}
}

func TestUsage(t *testing.T) {
	defer cleanup()

	var (
		i int
		s string
	)

	Var(NewValueVar(&i, 0), "VAR1", "Read me.")
	Var(NewValueVar(&s, "x"), "VAR2", "Read me too.")

	exp := "  VAR1 int\n" +
		"    \tRead me.\n" +
		"  VAR2 string\n" +
		"    \tRead me too. (default \"x\")"
	if Usage() != exp {
		t.Errorf("Usage incorrect: expected %q, got %q", exp, Usage())
	}
}

func TestVisitAll(t *testing.T) {
	defer cleanup()

	t.Setenv("A", "-1")

	Add("A", 0, "A")
	Add("B", 0, "B")

	Parse()

	var got string

	VisitAll(func(e *Env) {
		got += fmt.Sprintf("%s=%s, ", e.Name, e.Value)
	})

	exp := "A=-1, B=0, "
	if exp != got {
		t.Errorf("Expected %q, got %q", exp, got)
	}
}
