package env_test

import (
	"flag"
	"os"
	"testing"
	"time"

	"gitlab.com/slxh/go/env"
)

func testEnv() *env.EnvSet {
	return testEnvErr(env.ReturnFirstError)
}

func testEnvErr(h env.ErrorHandling) *env.EnvSet {
	var (
		i int
		d time.Duration
	)

	s := env.NewEnvSet("TEST_", h)
	s.Add("A", env.NewValueVar(&i, 0), "A number")
	s.Add("B", env.NewValueVar(&d, 0), "A duration")

	return s
}

func TestEnvSet_Lookup(t *testing.T) {
	if e := testEnv().Lookup("not defined"); e != nil {
		t.Errorf("Expected an undefined variable to return nil, but got %#v", e)
	}
}

func TestEnvSet_Output(t *testing.T) {
	s := testEnv()
	if s.Output() != os.Stderr {
		t.Errorf("Expected Output to return Stderr, but got %#v", s.Output())
	}
}

func TestEnvSet_Parse_ErrorHandling(t *testing.T) {
	t.Setenv("TEST_A", "not a number")
	t.Setenv("TEST_B", "1 jiffy")

	err := testEnvErr(env.ReturnFirstError).Parse()
	if err == nil {
		t.Errorf("Incorrect error: %s", err)
	}

	err = testEnvErr(env.ReturnLastError).Parse()
	if err == nil {
		t.Errorf("Incorrect error: %s", err)
	}
}

func TestEnvSet_ParseWithFlagSet(t *testing.T) {
	fs := flag.NewFlagSet("test", flag.ContinueOnError)
	fs.Duration("test-a", 0, "")

	s := testEnv()
	if err := s.ParseWithFlagSet(fs, nil); err != nil {
		t.Errorf("Unexpected error: %s", err)
	}

	if err := s.ParseWithFlagSet(fs, []string{"-test-a=1j"}); err == nil {
		t.Errorf("Expected error, got none")
	}

	t.Setenv("TEST_A", "1 jiffy")

	if err := s.ParseWithFlagSet(fs, nil); err == nil {
		t.Errorf("Expected error, got none")
	}
}

func TestEnvSet_Set(t *testing.T) {
	if err := testEnv().Set("B", "1 jiffy"); err == nil {
		t.Errorf("Expected error, got none")
	}

	if err := testEnv().Set("does not exist", "1 jiffy"); err == nil {
		t.Errorf("Expected error, got none")
	}
}
